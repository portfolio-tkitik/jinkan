package com.portfolio.jinkan.common;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import lombok.Generated;

@Component
public class Utility {

    /**
     * クッキー値取得。
     * 
     * @param cookies クッキー
     * @param name    クッキー名
     * @return クッキー値
     */
    public Optional<String> getCookieByName(Cookie[] cookies, String name) {
        Stream<Cookie> stream = Arrays.stream(Optional.ofNullable(cookies).orElse(new Cookie[] {}));
        Optional<String> value = stream.filter(c -> name.equals(c.getName())).findFirst().map(c -> c.getValue());
        return value;
    }

    /**
     * クッキー値設定。
     * 
     * @param name     クッキー名
     * @param value    クッキー値
     * @param response HttpServletResponse
     */
    public void setCookie(String name, String value, HttpServletResponse response) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    /**
     * システム日付取得。
     * 
     * @return 本日日付
     */
    @Generated
    public Date getSysDate() {
        return new Date();
    }
}
