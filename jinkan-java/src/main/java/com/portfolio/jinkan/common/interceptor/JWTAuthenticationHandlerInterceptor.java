package com.portfolio.jinkan.common.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.portfolio.jinkan.common.JWTProvider;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Component
public class JWTAuthenticationHandlerInterceptor implements HandlerInterceptor {

    private final JWTProvider provider;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws IOException {
        log.info("【SERVICE-START】" + request.getRequestURI());
        log.info("【AUTHORIZE-START】" + request.getRequestURI());
        String token = this.provider.getToken(request);
        log.info("token : " + token);
        if (this.provider.validateToken(token)) {
            String newToken = this.provider.refreshToken(token);
            this.provider.setToken(response, newToken);
            log.info("【AUTHORIZE-END】" + request.getRequestURI());
            response.setStatus(HttpStatus.OK.value());
            return true;
        }
        response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) {
        log.info("【SERVICE-END】" + request.getRequestURI());
    }
}
