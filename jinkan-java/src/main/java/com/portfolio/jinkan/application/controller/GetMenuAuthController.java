package com.portfolio.jinkan.application.controller;

import com.portfolio.jinkan.application.payload.MenuAuthResponse;
import com.portfolio.jinkan.domain.service.GetMenuInfoService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequestMapping("/api")
@RequiredArgsConstructor
@RestController
public class GetMenuAuthController {

    private final GetMenuInfoService service;

    /**
     * メニュー権限リスト取得。
     * 
     * @param menuTitle メニュータイトル
     * @return メニュー権限情報
     */
    @GetMapping("/get-menu-auth")
    public MenuAuthResponse getMenuAuthList(@RequestParam(name = "menuTitle") String menuTitle) {
        return MenuAuthResponse.builder()
            .authList(this.service.getMenuAuthList(menuTitle))
            .build();
    }
}
