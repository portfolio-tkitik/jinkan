package com.portfolio.jinkan.application.payload;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MenuInfo {

    /** メニューID */
    private String id;

    /** メニューURL */
    private String url;

    /** メニューアイコンPATH */
    private String path;

    /** メニュータイトル */
    private String title;
}
