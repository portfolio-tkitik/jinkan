package com.portfolio.jinkan.domein.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.portfolio.jinkan.domain.model.MenuInfoModel;
import com.portfolio.jinkan.domain.repository.MstMenuRepository;
import com.portfolio.jinkan.domain.service.GetMenuInfoService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetMenuInfoServiceTest {

    @InjectMocks
    private GetMenuInfoService target;

    @Mock
    private MstMenuRepository repository;

    @Test
    @DisplayName("メニュータイトルに紐づくメニュー権限を正常に取得できること")
    void getMenuAuthListTest() {

        // setup
        String menuTitle = "ホーム";
        String expectedAuth1 = "ROLE_ADMIN";
        String expectedAuth2 = "ROLE_USER";
        List<String> authorityList = Arrays.asList(expectedAuth1, expectedAuth2);
        doReturn(authorityList).when(this.repository).getAuthorityList(menuTitle);

        // execute
        List<String> actualList = this.target.getMenuAuthList(menuTitle);

        // assert
        assertAll(() -> {
            assertEquals(2, actualList.size());
        }, () -> {
            assertEquals(expectedAuth1, actualList.get(0));
        }, () -> {
            assertEquals(expectedAuth2, actualList.get(1));
        }, () -> {
            verify(this.repository, times(1)).getAuthorityList(menuTitle);
        });
    }

    @Test
    @DisplayName("権限に紐づくメニュー情報を正常に取得できること")
    void Test() {

        // setup
        String auth = "ROLE_USER";
        String expectedId1 = "001";
        String expectedUrl1 = "/";
        String expectedPath1 = "M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6";
        String expectedTitle1 = "ホーム";
        String expectedId2 = "002";
        String expectedUrl2 = "/employee-search";
        String expectedPath2 = "M8 16l2.879-2.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242zM21 12a9 9 0 11-18 0 9 9 0 0118 0z";
        String expectedTitle2 = "従業員検索";
        doReturn(this.createMenuInfoModelListStub()).when(this.repository).getMenuInfoList(auth);

        // execute
        List<MenuInfoModel> actualList = this.target.getMenuInfoList(auth);

        // assert
        assertAll(() -> {
            assertEquals(2, actualList.size());
        }, () -> {
            assertEquals(expectedId1, actualList.get(0).getId());
        }, () -> {
            assertEquals(expectedUrl1, actualList.get(0).getUrl());
        }, () -> {
            assertEquals(expectedPath1, actualList.get(0).getPath());
        }, () -> {
            assertEquals(expectedTitle1, actualList.get(0).getTitle());
        }, () -> {
            assertEquals(expectedId2, actualList.get(1).getId());
        }, () -> {
            assertEquals(expectedUrl2, actualList.get(1).getUrl());
        }, () -> {
            assertEquals(expectedPath2, actualList.get(1).getPath());
        }, () -> {
            assertEquals(expectedTitle2, actualList.get(1).getTitle());
        }, () -> {
            verify(this.repository, times(1)).getMenuInfoList(auth);
        });
    }

    private List<MenuInfoModel> createMenuInfoModelListStub() {
        List<MenuInfoModel> stubList = new ArrayList<>();
        stubList.add(
            MenuInfoModel.builder()
                .id("001")
                .url("/")
                .path("M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6")
                .title("ホーム")
                .build()
        );
        stubList.add(
            MenuInfoModel.builder()
                .id("002")
                .url("/employee-search")
                .path("M8 16l2.879-2.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242zM21 12a9 9 0 11-18 0 9 9 0 0118 0z")
                .title("従業員検索")
                .build()
        );
        return stubList;
    }
}
