package com.portfolio.jinkan.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import javax.servlet.http.Cookie;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

@SpringBootTest
public class UtilityTest {

    @Autowired
    private Utility target;

    @Test
    @DisplayName("Cookie有り：クッキー名に紐づくクッキー値を返却すること")
    void getCookieExistTest() {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        String cookieName = "token";
        String cookieValue = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEzNTAwMDAsImV4cCI6MTYyMTQzNjQwMH0.N35vsEF4AfyhI2SsV-4_Aon9OTwV8IxxuXnWklB7Z7c";
        request.setCookies(new Cookie(cookieName, cookieValue));

        // execute
        Optional<String> actual = this.target.getCookieByName(request.getCookies(), cookieName);

        // assert
        assertEquals(Optional.of(cookieValue), actual);
    }

    @Test
    @DisplayName("Cookie無し：Optional.empty()を返却すること")
    void getCookieNoneTest() {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        String cookieName = "token";

        // execute
        Optional<String> actual = this.target.getCookieByName(request.getCookies(), cookieName);

        // assert
        assertEquals(Optional.empty(), actual);
    }

    @Test
    @DisplayName("Cookieに引数の値が正常に設定されること")
    void setCookieTest() {

        // setup
        MockHttpServletResponse response = new MockHttpServletResponse();
        String cookieName = "token";
        String cookieValue = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEzNTAwMDAsImV4cCI6MTYyMTQzNjQwMH0.N35vsEF4AfyhI2SsV-4_Aon9OTwV8IxxuXnWklB7Z7c";

        // execute
        this.target.setCookie(cookieName, cookieValue, response);

        // assert
        assertEquals(cookieValue, response.getCookie(cookieName).getValue());
    }
}
