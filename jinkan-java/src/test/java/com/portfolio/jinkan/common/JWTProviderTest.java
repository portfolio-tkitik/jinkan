package com.portfolio.jinkan.common;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import javax.servlet.http.Cookie;

import com.portfolio.jinkan.common.configuration.AuthorizeConfiguration;

import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

@SpringBootTest
public class JWTProviderTest {

    @InjectMocks
    private JWTProvider target;

    @Mock
    private AuthorizeConfiguration configuration;

    @Mock
    private Utility utility;

    @Test
    @DisplayName("JWT有効期限内：true返却")
    void validateTokenNonExpiredTest() throws Exception {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String jwt2121 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEwNjEwNzEsImV4cCI6NDc3NTA2MTA3MX0.5-SeAcp5dc17e2epjgb2C5vkWLTdFsSzk7p9h07qUs0";
        doReturn("SECRET_KEY").when(this.configuration).getTokenSecretKey();
        doReturn(sdf.parse("2021-05-20 00:00:00")).when(this.utility).getSysDate();

        // execute
        boolean actual = this.target.validateToken(jwt2121);

        // assert
        assertAll(() -> {
            assertTrue(actual);
        }, () -> {
            verify(this.configuration, times(1)).getTokenSecretKey();
        }, () -> {
            verify(this.utility, times(1)).getSysDate();
        });
    }

    @Test
    @DisplayName("JWT有効期限切れ：false返却")
    void validateTokenExpiredTest() throws Exception {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String jwt21210426 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEwNjEwNzEsImV4cCI6NDc3NTA2MTA3MX0.5-SeAcp5dc17e2epjgb2C5vkWLTdFsSzk7p9h07qUs0";
        doReturn("SECRET_KEY").when(this.configuration).getTokenSecretKey();
        doReturn(sdf.parse("2121-04-27 00:00:00")).when(this.utility).getSysDate();

        // execute
        boolean actual = this.target.validateToken(jwt21210426);

        // assert
        assertAll(() -> {
            assertFalse(actual);
        }, () -> {
            verify(this.configuration, times(1)).getTokenSecretKey();
        }, () -> {
            verify(this.utility, times(1)).getSysDate();
        });
    }

    @Test
    @DisplayName("例外発生：false返却")
    void validateTokenErrorTest() throws Exception {

        // execute
        boolean actual = this.target.validateToken(null);

        // assert
        assertAll(() -> {
            assertFalse(actual);
        }, () -> {
            verify(this.configuration, times(1)).getTokenSecretKey();
        }, () -> {
            verify(this.utility, times(0)).getSysDate();
        });
    }

    @Test
    @DisplayName("JWTが正常に更新されること")
    void refreshTokenTest() throws ParseException {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String jwt2121 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEwNjEwNzEsImV4cCI6NDc3NTA2MTA3MX0.5-SeAcp5dc17e2epjgb2C5vkWLTdFsSzk7p9h07qUs0";
        String jwt20210520 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEzNTAwMDAsImV4cCI6MTYyMTQzNjQwMH0.N35vsEF4AfyhI2SsV-4_Aon9OTwV8IxxuXnWklB7Z7c";
        doReturn(sdf.parse("2021-05-19 00:00:00")).when(this.utility).getSysDate();
        doReturn("SECRET_KEY").when(this.configuration).getTokenSecretKey();
        doReturn(86400000l).when(this.configuration).getTokenValidDuration();

        // execute
        String actual = this.target.refreshToken(jwt2121);

        // assert
        assertAll(() -> {
            assertEquals(jwt20210520, actual);
        }, () -> {
            verify(this.utility, times(2)).getSysDate();
        }, () -> {
            verify(this.configuration, times(2)).getTokenSecretKey();
        }, () -> {
            verify(this.configuration, times(1)).getTokenValidDuration();
        });
    }

    @Test
    @DisplayName("Token存在：CookieからTokenを正常に取得できること")
    void getTokenTest() {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        String jwt20210520 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEzNTAwMDAsImV4cCI6MTYyMTQzNjQwMH0.N35vsEF4AfyhI2SsV-4_Aon9OTwV8IxxuXnWklB7Z7c";
        request.setCookies(new Cookie("token", jwt20210520));
        doReturn(Optional.of(jwt20210520)).when(this.utility).getCookieByName(request.getCookies(), "token");

        // execute
        String actual = this.target.getToken(request);

        // assert
        assertAll(() -> {
            assertEquals(jwt20210520, actual);
        }, () -> {
            verify(this.utility, times(1)).getCookieByName(request.getCookies(), "token");
        });
    }

    @Test
    @DisplayName("Token無し：空を返却すること")
    void getTokenNoneTest() {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        doReturn(Optional.empty()).when(this.utility).getCookieByName(request.getCookies(), "token");

        // execute
        String actual = this.target.getToken(request);

        // assert
        assertAll(() -> {
            assertEquals(StringUtils.EMPTY, actual);
        }, () -> {
            verify(this.utility, times(1)).getCookieByName(request.getCookies(), "token");
        });
    }

    @Test
    @DisplayName("正常にUtility.setCookieメソッドを呼び出すこと")
    void setTokenTest() {

        // setup
        MockHttpServletResponse response = new MockHttpServletResponse();
        String jwt20210520 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEzNTAwMDAsImV4cCI6MTYyMTQzNjQwMH0.N35vsEF4AfyhI2SsV-4_Aon9OTwV8IxxuXnWklB7Z7c";
        doNothing().when(this.utility).setCookie("token", jwt20210520, response);

        // execute
        this.target.setToken(response, jwt20210520);

        // assert
        verify(this.utility, times(1)).setCookie("token", jwt20210520, response);
    }
}
