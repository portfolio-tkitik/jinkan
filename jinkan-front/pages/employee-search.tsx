import { GetStaticProps } from 'next'
import GetMenuAuth from '../lib/GetMenuAuth'

const EmployeeSearch: React.FC = () => {
  return (
    <div className="flex justify-center items-center flex-col h-full">
      現在準備中
    </div>
  )
}

export default EmployeeSearch

export const getStaticProps: GetStaticProps = async () => {
  const title = '従業員検索'
  const authList: Array<string> = await GetMenuAuth(title)
  return {
    props: {
      layout: true,
      title: title,
      authList: authList,
    },
  }
}
