import { GetStaticProps } from 'next'
import GetMenuAuth from '../lib/GetMenuAuth'

const Settings: React.FC = () => {
  return (
    <div className="flex justify-center items-center flex-col h-full font-mono">
      現在準備中
    </div>
  )
}

export default Settings

export const getStaticProps: GetStaticProps = async () => {
  const title = '設定'
  const authList: Array<string> = await GetMenuAuth(title)
  return {
    props: {
      layout: true,
      title: title,
      authList: authList,
    },
  }
}
