import { GetStaticProps } from 'next'
import GetMenuAuth from '../lib/GetMenuAuth'

const CustomerBadget: React.FC = () => {
  return (
    <div className="flex justify-center items-center flex-col h-full">
      現在準備中
    </div>
  )
}

export default CustomerBadget

export const getStaticProps: GetStaticProps = async () => {
  const title = '顧客別予算管理'
  const authList: Array<string> = await GetMenuAuth(title)
  return {
    props: {
      layout: true,
      title: title,
      authList: authList,
    },
  }
}
