import { useState } from 'react'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import Cookie from 'universal-cookie'
import axios from 'axios'

const cookie = new Cookie()

interface Props {
  randomInt: number
}

const Login: React.FC<Props> = ({ randomInt }) => {
  const router = useRouter()
  const [userid, setUserid] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState('')

  const login = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    let params = new URLSearchParams()
    params.append('id', userid)
    params.append('pass', password)
    setIsLoading(true)
    try {
      const res = await axios.post(
        `${process.env.NEXT_PUBLIC_AUTH_RESTAPI_URL}/login`,
        params
      )
      if (res.status === 200) {
        console.log('res', res)
        const optionsClient = {
          path: '/',
        }
        const optionsRestApi = {
          path: '/',
          domain: process.env.NEXT_PUBLIC_RESTAPI_DOMAIN,
        }
        cookie.set('token', res.data.token, optionsClient)
        cookie.set('token', res.data.token, optionsRestApi)
        router.push('/')
      }
    } catch (error) {
      if (error.response) {
        if (error.response.status === 401) {
          setError('ログインできません')
        } else {
          setError(`Login Api Error : ${error.response.status}`)
        }
      } else {
        setError(`Error : ${error.message}`)
      }
    }
    setIsLoading(false)
  }

  return (
    <>
      <Head>
        <title>ログイン</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="tki_tik" />
      </Head>
      <div className="bg-gray-200 h-screen">
        <div className="w-full flex flex-wrap">
          <div className="w-full md:w-1/2 flex flex-col">
            {isLoading && (
              <div className="w-full md:w-1/2 h-full flex flex-col absolute z-10 bg-gray-300 bg-opacity-50">
                <div className="w-12 h-12 m-auto">
                  <Image
                    src={'/img/loading.gif'}
                    width={48}
                    height={48}
                    layout={'fixed'}
                  />
                </div>
              </div>
            )}
            <div className="flex flex-col justify-center md:justify-start my-auto pt-36 md:pt-0 px-8 md:px-24 lg:px-32">
              <Image
                src={'/img/jinkan-icon.svg'}
                alt={'jinkan-icon'}
                width={320}
                height={72}
              />
              <form className="flex flex-col" onSubmit={login}>
                <div className="flex flex-col pt-4">
                  <label htmlFor="id" className="text-lg">
                    ID
                  </label>
                  <input
                    type="text"
                    id="id"
                    value={userid}
                    placeholder="123456"
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                    onChange={(e) => setUserid(e.target.value)}
                  />
                </div>
                <div className="flex flex-col pt-4">
                  <label htmlFor="password" className="text-lg">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    value={password}
                    placeholder="Password"
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
                <input
                  type="submit"
                  value="Log In"
                  className="disabled:opacity-40 bg-gray-800 text-white font-bold text-lg hover:bg-gray-900 p-2 mt-8"
                  disabled={!userid || !password}
                />
              </form>
              {error && (
                <p className="mt-5 w-full text-center text-red-600">{error}</p>
              )}
            </div>
          </div>
          <div className="w-1/2 shadow-2xl">
            <img
              className="object-cover w-full h-screen hidden md:block"
              src={`/img/login_bg_${randomInt}.jpg`}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default Login

export const getServerSideProps: GetServerSideProps = async () => {
  const randomInt: number = Math.floor(Math.random() * 10) + 1
  return {
    props: {
      randomInt,
    },
  }
}
