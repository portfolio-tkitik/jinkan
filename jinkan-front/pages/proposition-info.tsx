import { GetStaticProps } from 'next'
import GetMenuAuth from '../lib/GetMenuAuth'

const PropositionInfo: React.FC = () => {
  return (
    <div className="flex justify-center items-center flex-col h-full">
      現在準備中
    </div>
  )
}

export default PropositionInfo

export const getStaticProps: GetStaticProps = async () => {
  const title = '案件情報'
  const authList: Array<string> = await GetMenuAuth(title)
  return {
    props: {
      layout: true,
      title: title,
      authList: authList,
    },
  }
}
