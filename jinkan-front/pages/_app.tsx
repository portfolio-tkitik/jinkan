import 'tailwindcss/tailwind.css'
import { AppProps } from 'next/app'
import Layout from '../components/Layout'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      {pageProps.layout ? (
        <Layout {...pageProps}>
          <Component {...pageProps} />
        </Layout>
      ) : (
        <Component {...pageProps} />
      )}
    </>
  )
}

export default MyApp
