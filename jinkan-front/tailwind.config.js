module.exports = {
  purge: ['./pages/**/*.tsx', './components/**/*.tsx'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    minWidth: {
      32: '8rem',
    },
  },
  variants: {
    extend: { opacity: ['disabled'] },
  },
  plugins: [],
}
