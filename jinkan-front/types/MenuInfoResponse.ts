import MenuInfo from './MenuInfo'

export default interface MenuInfoResponse {
  menuInfoList: Array<MenuInfo>
}
