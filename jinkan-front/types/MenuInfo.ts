export default interface MenuInfo {
  id: string
  url: string
  path: string
  title: string
}
