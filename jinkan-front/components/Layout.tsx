import { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import Cookie from 'universal-cookie'
import Header from './Header'
import Slidebar from './Slidebar'
import { callApi, httpClient } from '../lib/CallApi'
import { getRole } from '../lib/GetAuthInfo'
import MenuInfoResponse from '../types/MenuInfoResponse'

const cookie = new Cookie()

interface Props {
  title: string
  authList: Array<string>
}

const Layout: React.FC<Props> = ({ children, title, authList }) => {
  const router = useRouter()
  const [isLogin, setIsLogin] = useState(false)
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [menuInfoList, setMenuInfoList] = useState([])

  const getMenuInfoList = async (userAuth: string) => {
    const req = () => {
      return httpClient.get(
        `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/get-menu-info?auth=${userAuth}`
      )
    }
    const initialData = {
      menuInfoList: [],
    }
    const res = await callApi<MenuInfoResponse>(req, initialData)
    setMenuInfoList(res.menuInfoList)
  }

  useEffect(() => {
    const token = cookie.get('token')

    if (token && authList.includes(getRole(token))) {
      setIsLogin(true)
      getMenuInfoList(getRole(token))
    } else {
      cookie.remove('token')
      router.push('/login')
    }
  }, [])

  useEffect(() => {
    // token更新
    setIsSidebarOpen(false)
  }, [title])

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="tki_tik" />
      </Head>
      <div className="bg-white h-screen text-gray-600 font-mono">
        {isLogin && (
          <div className="flex h-screen bg-gray-200">
            <div
              onClick={() => setIsSidebarOpen(false)}
              className={`fixed z-20 inset-0 bg-black opacity-50 transition-opacity lg:hidden ${
                isSidebarOpen ? 'block' : 'hidden'
              }`}
            ></div>
            <Slidebar
              isOpen={isSidebarOpen}
              title={title}
              menuInfoList={menuInfoList}
            />
            <div className="flex-1 flex flex-col overflow-hidden">
              <Header title={title} setIsSidebarOpen={setIsSidebarOpen} />
              <main className="flex-1 overflow-x-hidden overflow-y-auto bg-gray-200">
                {children}
              </main>
            </div>
          </div>
        )}
      </div>
    </>
  )
}

export default Layout
