import { useEffect, useState } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import Cookie from 'universal-cookie'
import { getUsername } from '../lib/GetAuthInfo'

const cookie = new Cookie()

interface Props {
  title: string
  setIsSidebarOpen: (isSidebarOpen: boolean) => void
}

const Header: React.FC<Props> = ({ title, setIsSidebarOpen }) => {
  const router = useRouter()
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)
  const [username, setUsername] = useState('')

  const logout = () => {
    cookie.remove('token')
    router.push('/login')
  }

  useEffect(() => {
    setUsername(getUsername(cookie.get('token')))
  }, [])

  return (
    <header className="flex justify-between items-center py-4 px-6 bg-white border-b-4 border-black">
      <div className="flex items-center">
        <button
          onClick={() => setIsSidebarOpen(true)}
          className="text-gray-500 focus:outline-none lg:hidden"
        >
          <svg
            className="h-6 w-6"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M4 6H20M4 12H20M4 18H11"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            ></path>
          </svg>
        </button>
      </div>
      <div className="lg:w-full lg:text-left text-xl">
        <span>{title}</span>
      </div>
      <div className="flex items-center">
        <div className="relative">
          <button
            onClick={() => setIsDropdownOpen(!isDropdownOpen)}
            className="relative block h-8 w-8 rounded-full overflow-hidden shadow focus:outline-none"
          >
            <Image src="/img/profile.svg" layout="fill" />
          </button>
          <div
            onClick={() => setIsDropdownOpen(false)}
            className={`fixed inset-0 h-full w-full z-10 ${
              isDropdownOpen ? '' : 'hidden'
            }`}
          ></div>
          <div
            className={`absolute min-w-32 right-0 mt-2 bg-white rounded-md overflow-hidden shadow-xl z-10 ${
              isDropdownOpen ? '' : 'hidden'
            }`}
          >
            <Link href="/">
              <a
                onClick={() => setIsDropdownOpen(false)}
                className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-900 hover:text-white"
              >
                {username}
              </a>
            </Link>
            <Link href="/settings">
              <a
                onClick={() => setIsDropdownOpen(false)}
                className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-900 hover:text-white"
              >
                Settings
              </a>
            </Link>
            <a
              onClick={logout}
              className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-900 hover:text-white"
            >
              Logout
            </a>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
