import AuthInfo from '../types/AuthInfo'

export const getUserId = (token: string): string => {
  return getPayload(token).sub
}

export const getUsername = (token: string): string => {
  return getPayload(token).username
}

export const getRole = (token: string): string => {
  return getPayload(token).role
}

const getPayload = (token: string): AuthInfo => {
  const base64Payload = token.split('.')[1].replace('-', '+').replace('_', '/')
  const decodePayload = decodeURIComponent(escape(atob(base64Payload)))
  const payload = JSON.parse(decodePayload)
  console.log(payload)
  return payload
}
