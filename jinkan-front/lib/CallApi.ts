import Router from 'next/router'
import axios, { AxiosResponse } from 'axios'

export const httpClient = axios.create({
  withCredentials: true,
})

export const callApi = async <T>(
  axiosFunc: () => Promise<AxiosResponse<T>>,
  initialData: T
): Promise<T> => {
  const res = await axiosFunc().catch((err) => {
    return err.response
  })
  if (res.status === 200) {
    console.log(`res : ${res.data}`)
    return res.data
  } else {
    Router.push('/login')
    return initialData
  }
}
