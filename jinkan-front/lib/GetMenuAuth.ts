import axios from 'axios'

const GetMenuAuth = async (menuTitle: string): Promise<Array<string>> => {
  const res = await axios.get(
    `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/get-menu-auth`,
    {
      params: {
        menuTitle: menuTitle,
      },
    }
  )
  return res.data.authList
}

export default GetMenuAuth
